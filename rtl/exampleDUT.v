// This is an example verilog code to get started with the very first time  
`define	RST_COUNT	5'b00000		// Counter Reset
`define RST 		2'b00
`define CHX		2'b01
`define CHY		2'b10
`define CHZ		2'b11

module exampleDUT( 
  input clk_in, 
  input rstn_in,
  input clk_sel,
  output clk_mux_out
);

	// Local Signal
   	wire 		wire1;		//Behaves like wire, no storage	
        wire [4:0]	max_count;
        wire 		mux_enbl;
	reg [1:0]	ch_indicator;
	reg  		clk_half;	//Register bit  
   	reg [1:0] 	reg_arr;	//Register array
        reg [1:0]	ch_state; 
	reg [1:0]	next_ch_ptr;
        reg [4:0]	pulse_cnt;
       

   assign mux_enbl = 1'b1;

   // ********************************************************
   // This code results in a clock divide by 2
   // It's positive edge trigerred by the input clock (clk_in) 
   // and negative edge triggered by the reset
   // ** ALWAYS inlcude a reset response for all clocked modules **  
   // ********************************************************
   always @(posedge clk_in or negedge rstn_in)
 	if (~rstn_in )
		clk_half <= 1'b0;
	else clk_half <= ~clk_half;

   // ********************************************************
   // This is combinatorial logic representing a MUX ie:
   // ********************************************************
   assign clk_mux_out = clk_sel ? clk_in : clk_half;

   // ********************************************************
   // THis is assigning a bus a hard coded value
   // ********************************************************
   assign max_count = 5'b10001;

   // ********************************************************
   // This is a an example of a 5-bit counter 
   // ********************************************************
   always @(posedge clk_in or negedge rstn_in) 
     if (~rstn_in)
       pulse_cnt <= `RST_COUNT; 
     else if (pulse_cnt == max_count)
       pulse_cnt <= `RST_COUNT;
     else
       pulse_cnt <= pulse_cnt + 1'b1;

   // ********************************************************
   // This is a simple state machine which rotates the 
   // state (ch_state) 00 -> 01 -> 10 -> 11
   // Simulataneously it changes the state 'next_ch_ptr' which 
   // points to the next state of ch_state
   // ********************************************************
   always @(posedge clk_in or negedge rstn_in)
     if (~rstn_in)
       {ch_state, next_ch_ptr} <= {`RST, `CHZ} ;
     else if (ch_state == `CHZ )
       {ch_state, next_ch_ptr} <= {`CHX, `CHY} ;
     else if (ch_state == `CHX )
       {ch_state, next_ch_ptr} <= {`CHY, `CHZ} ;
     else if (ch_state == `CHY )
       {ch_state, next_ch_ptr} <= {`CHZ, `CHX} ;
     else if (ch_state == `RST )
       {ch_state, next_ch_ptr} <= {`CHZ, `CHX} ;


   // ********************************************************
   // This is a combinatorial logic which is implemented using a 
   // always @(*) block and if-else statement 
   // **NOTE** if-else if prioritizes the execution of the statement.
   // ********************************************************
   always @(*)	//@(*) automatically puts all the signals used in the logic as the sensitivity list.
	if (mux_enbl & (ch_state == `CHZ)) ch_indicator <= `CHZ;
	else if (mux_enbl & (ch_state == `CHX)) ch_indicator <= `CHX;
	else if (mux_enbl & (ch_state == `CHY)) ch_indicator <= `CHY;
	else ch_indicator <= `RST;

endmodule
