//
// THis is the verilog file used for providing stimulus to exampleDUT.v

`timescale 1ns/1ps

module stimulus_example(
	output reg clk_out,
	output reg rstn_out,
        output reg clk_sel,
        input wire clk_mux
);

//Contains all options for storing and viewing data
`include "../tb/wave_default.v"

// A task to generate a reset signal and define end of simulation time. 
task testRun;
begin
   	#50   rstn_out = 1'b0;		//#100 -> 100ps
   	#200  rstn_out = 1'b1;
        #5000  clk_sel = 1'b1;
   	#10000;
end
endtask  

// Clock generation for clock_gen; 
initial clk_out=0;
initial forever #250 clk_out=~clk_out;

initial rstn_out = 1'b0;
initial clk_sel = 1'b0;

// main test loop; all we need to do is reset here, input clock will drive output clocks creation
initial begin
	testRun;
	$finish(2);
end

endmodule
