//TB_example.v 
//
// Copyright 2018 Silicon Institute of Technology, Bhubaneswar, India 
//

`timescale 1ns/1ps

module TB_example();

// Instiatiate the DUT (exampleDUT)
exampleDUT IDUT(
.clk_in(clk),
.rstn_in(rstn),
.clk_sel(clk_sel),
.clk_mux_out(clk_mux)
);

// Instatiate the stimulus module.
stimulus_example Istim(
.clk_out(clk),
.rstn_out(rstn),
.clk_sel(clk_sel),
.clk_mux(clk_mux)
);


endmodule
