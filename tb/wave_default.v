
// This file uses the $shm_probe system task to specify the signals whose value changes 
// you want to recodrd in your SHM database.
// The syntax for the $shm-probe system task is as follows:
// $shm_probe [(scope1, "node_specifier1", scope2, "mode_specifier2", ... )];
//
// Node Specifier Character
// "A" - All nodes (including inputs, outputs, and inouts) in the specified scope, 
//       excluding memories.
// "S" - Inputs, outputs, and inouts in the specified scope, and in all instantiations 
//       below the specified scope, except nodes inside library cells.
// "C" - Inputs, outputs, and inouts in the specified scope, and in all instantiations 
//       below the specified scope, including nodes inside library cells.
// "M" - Inputs, outputs, inouts and memeories in the specified scope.
// "T" - Objects declared in task scope.  By default, task scopes are not included 
//       unless the specified scope is a task scope.
// "F" - Objects declared in function scopes. By default, function scopes are not included 
//       unless the specified scope is a function scope.
//
//-------- Setup Waveform Capture --------   
initial
  begin
     $display("Using the wave_default.v to capture waveforms. Sent to ");
     $shm_open({"waveforms.shm"});
     $shm_probe(TB_example, "ASCMTF");
//   $shm_probe("AC");
//   $shm_probe("ACMT");
  end
